package com.serviceheroes.bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

@Service
public class CasinoService {
    @Autowired
    private BankService bankService;

    public String gamble(Map<String, Object> body) {
        String amount = (String) body.get("gamble");
        System.out.println(amount);
        Transfer transfer = new Transfer();
        final Random random = new Random();
        transfer.setType("gamble");
        transfer.setReason("gambling");
        if (random.nextBoolean()) {
            transfer.setAmount(Float.parseFloat(amount));
        } else {
            transfer.setAmount(-1 * Float.parseFloat(amount));
        }
        bankService.handleTransfer(transfer);
        return "thank you for gambling " + amount + " euros";
    }

}
