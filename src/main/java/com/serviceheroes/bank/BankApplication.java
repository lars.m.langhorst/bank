package com.serviceheroes.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankApplication {

	public static void main(String[] args) {
		BankAccount bankAccount = new BankAccount();
		SpringApplication.run(BankApplication.class, args);

	}

}
