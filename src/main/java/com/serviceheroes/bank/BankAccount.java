package com.serviceheroes.bank;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class BankAccount {

    private static float balance = 100;
    private final ArrayList<Transfer> transfers = new ArrayList<>();
    public void handleTransfer(Transfer transfer){
        transfers.add(transfer);
        if (transfer.getType().equals("withdraw")){
            setBalance(getBalance() - transfer.getAmount());
        }
        if (transfer.getType().equals("deposit")){
            setBalance(getBalance() + transfer.getAmount());
        }
    }
    public ArrayList<Transfer> getTransfers() {
        return transfers;
    }
    public void addTransfer(Transfer transfer) {
        transfers.add(transfer);
    }
    public float getBalance() {
        return balance;
    }
    public void setBalance(float amount) {
        balance = amount;
    }
}



