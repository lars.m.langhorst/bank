package com.serviceheroes.bank;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/bank")
public class BankController {
    private String status = "home";
    @Autowired
    private BankService bankService;
    @GetMapping("/")
    public String index() {
        return "what would you like to do?";
    }
    @GetMapping("/history")
    public Map<String, Object> history() {
        return Collections.singletonMap("transfers", bankService.getHistory());
    }
    @GetMapping("/balance")
    public Map<String, Object> balance() {
        return Collections.singletonMap("balance", bankService.getBalance());
    }
    @PostMapping("/action")
    public String GetBankAction(@RequestBody Map<String, Object> body) {
        if ((body.containsKey("action")) && ((String)body.get("action")).contains("deposit")) {
            status = "deposit";
            return "how much to deposit";
        }
        if ((body.containsKey("action")) && ((String)body.get("action")).contains("withdraw")) {
            status = "withdraw";
            return "how much to withdraw";
        }
        if ((body.containsKey("deposit"))) {
            return bankService.deposit(body);
        }
        if ((body.containsKey("withdraw"))) {
            return bankService.withdraw(body);
        }
        if ((body.containsKey("action")) && ((String)body.get("action")).contains("history")) {
            return "go to /bank/history";
        }
        else {
            return "choose action";
        }
    }


//request param(get/delete), request body(post/patch)

}
