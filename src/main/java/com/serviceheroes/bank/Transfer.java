package com.serviceheroes.bank;

public class Transfer{

    private String type;
    private float amount;
    private String reason;
    private String date;
    //constructor same name as class, no return type, no void
    public Transfer() {}

    public String getType() {
        return type;
    }
    public float getAmount() {
        return amount;
    }
    public String getReason() {
        return reason;
    }
    public String getDate() {
        return date;
    }

    // void because does not return a value
    public void setType(String type){
        this.type = type;
    }
    public void setAmount(float amount){
        this.amount = amount;
    }
    public void setReason(String reason) {
        this.reason = reason;
    }
    public void setDate(String date) {
        this.date = date;
    }

}
