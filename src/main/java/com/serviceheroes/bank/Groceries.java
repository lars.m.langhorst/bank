package com.serviceheroes.bank;
import java.util.HashMap;
public class Groceries {
    HashMap<String, Integer> groceryList = new HashMap<>();
    public Groceries() {
        groceryList.put("milk", 50);
        groceryList.put("ham", 60);
        groceryList.put("cheese", 30);
        groceryList.put("beer", 20);
        groceryList.put("bread", 80);
        groceryList.put("wine", 26);
        groceryList.put("salami", 25);
        groceryList.put("whey", 52);
        groceryList.put("chips", 55);
    }
    public Integer getPrice(String key) {
        return groceryList.get(key);
    }
}
