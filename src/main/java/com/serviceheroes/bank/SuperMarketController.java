package com.serviceheroes.bank;
import com.sun.deploy.cache.BaseLocalApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
@RestController
@RequestMapping("/supermarket")
public class SuperMarketController {
    private String status = "supermarket";
    @Autowired
    private SupermarketService supermarketService;
    Groceries groceries = new Groceries();
    @GetMapping("/")
    public String index() {
        return "What would you like to do?";
    }
    @PostMapping("/price")
    public String Getprice(@RequestBody Map<String, Object> body) {
        String product = (String) body.get("price");
        if (groceries.groceryList.containsKey(product)){
        return "the price is " + groceries.getPrice(product);
        }else{
            return "please choose a product";
        }
    }
    @PostMapping("/buy")
    public String DoGroceries(BankAccount bankAccount, @RequestBody Map<String, Object> body) {
        String product = (String) body.get("buy");
        if (groceries.groceryList.containsKey(product)){
            supermarketService.groceries(groceries.getPrice(product));
            return "you bought " + (String) body.get("buy") + " for " + groceries.getPrice(product);
        }else{
            return "please choose a product";
        }
    }
}


