package com.serviceheroes.bank;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/casino")
public class CasinoController {
    private String status = "casino";
    @Autowired
    private CasinoService casinoService;
    @GetMapping("/")
    public String index() {
        return "What would you like to do?";
    }
    @PostMapping("/gamble")
    public String GetBankAction(BankAccount bankAccount, @RequestBody Map<String, Object> body) {
        if ((body.containsKey("gamble"))) {
            return casinoService.gamble(body);
        }
        else {
            return "choose action";
        }
    }
}
