package com.serviceheroes.bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Random;

@Service
public class SupermarketService {
    @Autowired
    private BankService bankService;
    private final String pattern = "hh:mm:ss dd-MM-yyyy";
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String date = simpleDateFormat.format(new Date());
    public void groceries(int price) {
        Transfer transfer = new Transfer();
        transfer.setAmount(price);
        transfer.setType("withdraw");
        transfer.setReason("groceries");
        transfer.setDate(date);
        bankService.handleTransfer(transfer);
    }
}
