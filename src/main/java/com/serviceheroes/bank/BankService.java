package com.serviceheroes.bank;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

@Service
public class BankService {
    BankAccount bankAccount = new BankAccount();
    public void handleTransfer(Transfer transfer){
        bankAccount.addTransfer(transfer);
        if (transfer.getType().equals("withdraw")){
            bankAccount.setBalance(getBalance() - transfer.getAmount());
        }
        if (transfer.getType().equals("deposit")){
            bankAccount.setBalance(getBalance() + transfer.getAmount());
        }
    }
    public ArrayList<Transfer> getHistory(){
        return bankAccount.getTransfers();
    }
    public float getBalance(){
        return bankAccount.getBalance();
    }

    public String deposit(Map<String, Object> body) {
        String amount = (String) body.get("deposit");
        System.out.println(amount);
        Transfer transfer = new Transfer();
        transfer.setAmount(Float.parseFloat(amount));
        transfer.setType("deposit");
        bankAccount.handleTransfer(transfer);
        return "thank you for deposit" + amount;
    }
    public String withdraw(Map<String, Object> body) {
        String amount = (String) body.get("withdraw");
        System.out.println(amount);
        Transfer transfer = new Transfer();
        transfer.setAmount(-1*Float.parseFloat(amount));
        transfer.setType("withdraw");
        bankAccount.handleTransfer(transfer);
        return "thank you for withdrawal" + amount;

    }
}
